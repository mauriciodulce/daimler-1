<?php 
include "include/header.php";
$cliente="mercedes";
?>
<div class="inner clientes">
     <div class="tituloheader"><h1>Mapa del sitio</h1></div>

    <!--CONTENIDOS--> 
    <div class="container content-interna sitemap">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
               <!--Compañía--> 
               <article class="row map">
                   <div class="col-md-12">
                       <div class="bannersecc" style="background-image: url('img/compania.jpg')">
                           <div class="titulo"><a href="compania.php">Compañía</a></div>
                       </div>
                   </div>
               </article>
               
               <!--Clientes--> 
               <article class="row map">
                   <div class="col-md-12">
                       <div class="bannersecc" style="background-image: url('img/header_MB.jpg'); background-position: center">
                           <div class="titulo"><a href="mercedes-benz.php">Clientes</a></div>
                       </div>
                       <div class="listado">
                           <ul>
                               <li><a href="mercedes-benz.php">Mercedes-Benz</a></li>
                               <li><a href="freightliner.php">Freightliner</a></li>
                               <li><a href="fuso.php">FUSO</a></li>
                               <li><a href="creacion-actualizacion-clientes.php">Creación y/o actualización de clientes</a></li>
                           
                           </ul>
                       </div>
                   </div>
               </article>
               
               <!--Proveedores--> 
               <article class="row map">
                   <div class="col-md-12">
                       <div class="bannersecc" style="background-image: url('img/creacionproveedores.jpg'); ">
                           <div class="titulo"><a href="condiciones-generales.php">Proveedores</a></div>
                       </div>
                       <div class="listado">
                           <ul>
                               <li><a href="condiciones-generales.php">Condiciones generales</a></li>
                               <li><a href="creacion-actualizacion-proveedores.php">Creación y/o actualización de proveedores</a></li>
                              
                           </ul>
                       </div>
                   </div>
               </article>
               
               <!--Concesionarios--> 
               <article class="row map">
                   <div class="col-md-12">
                       <div class="bannersecc" style="background-image: url('img/concesionarios.jpg')">
                           <div class="titulo"><a href="concesionarios.php">Concesionarios</a></div>
                       </div>
                   </div>
               </article>
               
               <!--Campaña--> 
               <article class="row map">
                   <div class="col-md-12">
                       <div class="bannersecc" style="background-image: url('img/campana.jpg');">
                           <div class="titulo camp"><a href="recall/index.php">Campaña de seguridad</a></div>
                       </div>
                       <div class="listado">
                            <div class="col-md-6">
                           <ul>
                               <li><a href="recall/clasec.php">Clase C</a></li>
                               <li><a href="recall/clasec-205.php">Clase C 205</a></li>
                               <li><a href="recall/clase-glk.php">GLK</a></li>
                               <li><a href="recall/clase-ml.php">Clase ML</a></li>
                               <li><a href="recall/clase-gla200-cla200-b180.php">Clase GLA-200, CLA-200 y B-180</a></li>
                               <li><a href="recall/cascadia.php">Cascadia</a></li>
                           </ul>
                            </div>
                           <div class="col-md-6">
                               <ul>
                               <li><a href="recall/m2-106-camara.php">Cámara de freno M2 106 6X4</a></li>
                               <li><a href="recall/m2-106-cinturon.php">M2106, Cambio de hebillas cinturón de seguridad</a></li>
                               <li><a href="recall/m2-112.php">M2 112 Año/modelo 2015</a></li>
                               <li><a href="recall/levas-escape.php">Arbol de levas escape</a></li>
                               <li><a href="recall/levas-admision.php">Arbol de levas admisión</a></li>
                               <li><a href="recall/slk-200.php">SLK 200</a></li>
                               
                             
                               
                           
                           </ul>
                           </div>
                       </div>
                   </div>
               </article>
               <!--Contacto--> 
               <article class="row map">
                   <div class="col-md-12">
                       <div class="bannersecc" style="background-image: url('img/contacto.jpg')">
                           <div class="titulo"><a href="contacto.php">Contacto</a></div>
                       </div>
                   </div>
               </article>
               
            </div>
            
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al inicio">Home</a><span class="sep">></span>
        <a href="compania.php" title="Ir a Compañía">Compañía</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>      