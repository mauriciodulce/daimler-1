<?php 
$cliente="freightliner";
include "include/header.php";
?>
<div class="inner clientes">
    <div class="tituloheader"><h1>Freightliner</h1></div>
    <!--BANNER INICIO-->
    <div class="cabecera freight" style="background-image: url('img/clientes/freightliner.jpg')"></div>

    <!--CONTENIDOS--> 
    <div class="container content-interna">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 ">
                <div class="cap1">
                    <h3>GARANTÍA DE REPUESTOS FREIGHTLINER Y DETROIT</h3>
                    
                </div>
                <div class="tablecont">
                <h3>TERMINOS DE COBERTURA</h3>
<h3>CONDICIONES GENERALES</h3>
<p style="font-size: 15px; text-align: left;">La garant&iacute;a otorgada por <strong>Daimler Colombia S.A</strong> cubre la reparaci&oacute;n o cambio de las piezas o componentes originales del veh&iacute;culo que presenten defectos de calidad o ensamble de f&aacute;brica, sin costo alguno para el propietario salvo el de los componentes que sufren desgaste o deterioro por el uso normal del veh&iacute;culo (Ver componentes no cubiertos).</p>
<h3>CONDICIONES ESPECIALES</h3>
<p>Daimler Colombia S.A. podr&aacute; sustituir o reparar los componentes o piezas reconocidas como defectuosas o mal ensambladas de f&aacute;brica en el veh&iacute;culo. Los gastos de mano de obra para la remoci&oacute;n e instalaci&oacute;n de los componentes, conjuntos o sistemas del veh&iacute;culo por garant&iacute;a, tambi&eacute;n ser&aacute;n cubiertos por la misma, siempre que tales trabajos se realicen en los talleres autorizados por Daimler Colombia S.A., de acuerdo con el listado del presente documento, que se actualizar&aacute; peri&oacute;dicamente en la p&aacute;gina web <a href="http://www.daimler.com.co">www.daimler.com.co</a>.</p>
<p>El propietario del veh&iacute;culo debe acreditar la realizaci&oacute;n de los servicios de conservaci&oacute;n y mantenimiento, se&ntilde;alando los mismos en el Cuaderno de Mantenimiento e instrucciones de Servicio que se aconseja sean realizados prioritariamente en un taller autorizado por Daimler Colombia S.A</p>
<p>En las reparaciones de garant&iacute;a &nbsp;por causas imputables a Daimler Colombia S.A. y/o a uno de los concesionarios autorizados, se interrumpir&aacute; autom&aacute;ticamente el plazo de la garant&iacute;a y dicho tiempo se computar&aacute; como prolongaci&oacute;n de la misma.</p>
<p>No est&aacute;n incluidas dentro de la garant&iacute;a las consecuencias indirectas de un posible defecto tales como las p&eacute;rdidas comerciales, indemnizaciones por el tiempo de inmovilizaci&oacute;n, lucro cesante, gasolina, tel&eacute;fonos, viajes, gastos por alquiler de un veh&iacute;culo, alojamientos, comidas, almacenaje, etc.</p>
<h3>EXCLUSIONES DE LA GARANTIA</h3>
<p>La garant&iacute;a perder&aacute;&nbsp; su validez cuando ocurra cualquiera de los siguientes casos:</p>
<p>Si no se realizan los servicios de conservaci&oacute;n, revisiones y mantenimiento obligatorios en los per&iacute;odos se&ntilde;alados en el Manual de Mantenimiento entregado al propietario siempre y cuando el evento reportado por el cliente est&eacute; relacionado directa o indirectamente a la falta o calidad del mantenimiento realizado.</p>
<p>&nbsp;Cuando se han producido da&ntilde;os en el veh&iacute;culo por &nbsp;no seguir las observaciones t&eacute;cnicas de Daimler Colombia S.A. relacionadas con (a.) el uso o manejo inadecuado del veh&iacute;culo, o (b.) servicios de conservaci&oacute;n y mantenimiento preventivos inadecuados y/o no efectuados de acuerdo a las condiciones de uso equivalentes (normales, mixtas y dif&iacute;ciles), as&iacute; como por lo citado en el Manual de Mantenimiento y el evento reportado est&eacute; relacionado con estas condiciones incumplidas.</p>
<p>Si los da&ntilde;os o mal funcionamiento del veh&iacute;culo se deben a uso inadecuado (Ej. sobrepeso, sobrecupo, peso sobre cada eje, dimensiones m&aacute;ximas), accidentes, vandalismo, condiciones ambientales adversas, desastres naturales y en general por caso fortuito o fuerza mayor.</p>
<p>Si el veh&iacute;culo ha sido utilizado en condiciones y prop&oacute;sitos distintos a&nbsp; las normales para las cuales fue dise&ntilde;ado o no ha sido operado siguiendo las recomendaciones del Manual de Operaci&oacute;n y el Manual de Mantenimiento entregados junto con el veh&iacute;culo y&nbsp; las dem&aacute;s instrucciones comunicadas por Daimler Colombia S.A.</p>
<p>Si la instalaci&oacute;n de carrocer&iacute;as, blindajes, equipos de volteo, furgones, tanques adicionales, accesorios, equipo electr&oacute;nico, &nbsp;alarmas u otros componentes afectan el buen funcionamiento u ocasionen da&ntilde;os en el veh&iacute;culo.</p>
<h3>MANTENIMIENTO</h3>
<p>La presente garant&iacute;a no cubre costos de mantenimiento de acuerdo a lo establecido en los manuales de operaci&oacute;n y conservaci&oacute;n del veh&iacute;culo, o&nbsp; por fallas debidas al uso de combustible, lubricantes y refrigerantes que no cumplan con las especificaciones recomendadas por Daimler Colombia S.A.</p>
<p>La realizaci&oacute;n de los mantenimientos establecidos en el manual del conductor y de mantenimiento y el uso de combustible, aceite, lubricantes, y refrigerantes apropiados es responsabilidad del propietario.</p>
<h3>PERIODO DE COBERTURA</h3>
<p>Esta garant&iacute;a aplica para el primer propietario y los due&ntilde;os subsiguientes y estar&aacute; vigente durante el periodo o el recorrido mencionados a continuaci&oacute;n, contados desde la fecha de entrega del veh&iacute;culo al primer propietario (estipulada en este certificado), y&nbsp; terminar&aacute; cuando se cumpla el plazo o alcance el kilometraje indicado, lo primero que ocurra y aplica para los veh&iacute;culos nuevos, importados o ensamblados por <strong>Daimler Colombia S.A, </strong>vendidos por la compa&ntilde;&iacute;a o por sus concesionarios autorizados, matriculados y usados&nbsp; en Colombia.</p>
<table class="table tabled tablebold">
<tbody>
<tr>
<td colspan="2">&nbsp;VIGENCIA DE GARANT&Iacute;A FREIGHTLINER</td>
</tr>
<tr>
<td colspan="2">&nbsp;CUBRIMIENTO GENERAL INCLUYE TREN DE FUERZA (CAJA Y EJES)</td>
</tr>
<tr>
<td>&nbsp;Tiempo</td>
<td>Kilometraje&nbsp;</td>
</tr>
<tr>
<td>&nbsp;18 meses</td>
<td>&nbsp;160.000 Km</td>
</tr>
<tr>
<td colspan="2">&nbsp;VIGENCIA DE GARANTIA MOTORES DETROIT DIESEL</td>
</tr>
<tr>
<td>&nbsp;MBE 900</td>
<td>&nbsp;36 Meses o 240.000 Km</td>
</tr>
<tr>
<td>&nbsp;SERIE 60</td>
<td>&nbsp;24 Meses sin l&iacute;mite de Km</td>
</tr>
<tr>
<td>&nbsp;DD15</td>
<td>&nbsp;24 Meses sin l&iacute;mite de Km</td>
</tr>
<tr>
<td>&nbsp;MBE 4000</td>
<td>&nbsp;24 Meses sin l&iacute;mite de Km</td>
</tr>
<tr>
<td>&nbsp;Inyectores de Combustible</td>
<td>&nbsp;160.000 Km (Solo serie 60 y MBE 4000)</td>
</tr>
</tbody>
</table>

<p>Todas las intervenciones realizadas al amparo de la Garant&iacute;a Contractual est&aacute;n garantizadas hasta el vencimiento del per&iacute;odo original de garant&iacute;a del veh&iacute;culo (tiempo o kilometraje). Para las reparaciones realizadas a partir de la finalizaci&oacute;n del mes 12 o repuestos comprados por mostrador aplica la siguiente vigencia.</p>
<p>&nbsp;</p>
<table class="table tabled tablebold">
<tbody>
<tr style="height: 49px;">
<td style="height: 49px;" colspan="2">GARANTIA DE REPUESTO CUBRIMIENTO GENERAL</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">Instalado en taller</td>
<td style="height: 49px;">Vendido por mostrador</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">6 Meses sin l&iacute;mite de Km</td>
<td style="height: 49px;">3 meses</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;" colspan="2">El plazo y/o kilometraje se contabilizar&aacute;, tomando como base la fecha y/o kilometraje registrado en la Factura</td>
</tr>
</tbody>
</table>
<table class="table tabled tablebold">
<tbody>
<tr>
<td colspan="2">GARANTIA DE REPUESTO MOTOR</td>
</tr>
<tr>
<td>Instalado en taller</td>
<td>Vendido por mostrador</td>
</tr>
<tr>
<td>12 meses sin l&iacute;mite de Km</td>
<td>6 meses</td>
</tr>
<tr>
<td colspan="2">El plazo y/o kilometraje se contabilizar&aacute;, tomando como base la fecha y/o kilometraje registrado en la Factura</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>

<p>Dentro de la garant&iacute;a de motor no se incluyen las siguientes piezas: Sistema de Aire acondicionado, Motor de arranque, Alternador, Embrague, Cardan y juntas de transmisi&oacute;n, Secador de aire.</p>
<h3>PIEZAS O COMPONENTES NO CUBIERTOS POR LA &nbsp;GARANT&Iacute;A</h3>
<p>La presente garant&iacute;a excluye componentes y piezas que presenten desgaste normal, deterioro &nbsp;o que sean de mantenimiento: correas, soportes de motor, empaques, disco de embrague, forros de embrague, prensas, sincronizadores, retenedores, filtro secador de aire, filtros del sistema de admisi&oacute;n, filtros del sistema de combustible, filtros del sistema de lubricaci&oacute;n, filtros del sistema neum&aacute;tico, filtro de la botella deshidratadora del sistema de A/C, lubricantes y refrigerantes, l&iacute;quido de frenos, fluidos del sistema hidr&aacute;ulico, fluidos de A/C, &nbsp;bandas y campanas de freno, retenes, bujes, rodamientos, terminales, amortiguadores, muelles, hojas de ballesta, &nbsp;fuelles neum&aacute;ticos, guayas &nbsp;y cables, &nbsp;plumillas, antenas, encendedor de cigarrillos, vidrios, herramientas, fusibles, reles, bombillos cables el&eacute;ctricos del trailer, tapa del radiador, mangueras neum&aacute;ticas del trailer, pintura de chasis.</p>
<p>La garant&iacute;a tampoco cubre mano de obra, alineaci&oacute;n&nbsp; y balanceo de ruedas, &nbsp;ni el mantenimiento o reajuste de las partes anteriormente &nbsp;mencionadas y otras que est&eacute;n dentro del mismo concepto de desgaste normal o de mantenimiento.</p>
<p>Los neum&aacute;ticos gozan de la garant&iacute;a que otorgan sus fabricantes acogi&eacute;ndose a las condiciones otorgadas por los mismos. En todo caso,&nbsp; las llantas estar&iacute;an exentas en caso que el da&ntilde;o sea consecuencia de golpes, pellizcamientos causados por golpes, desgastes prematuros por falta o fallas en la alineaci&oacute;n y el uso de una presi&oacute;n de inflado incorrecta. En caso de cualquier reclamaci&oacute;n por Garant&iacute;a, el concesionario de su preferencia est&aacute; a disposici&oacute;n para encaminar su solicitud al representante local de la marca de los neum&aacute;ticos de su veh&iacute;culo.</p>
<h3>CUBRIMIENTOS ESPECIFICOS</h3>
<p><strong>&nbsp;1.&nbsp;</strong><strong >BATERIAS</strong></p>
<p>Cobertura: 12 meses &oacute; 160.000 Kms</p>
<p>Esto aplica &uacute;nicamente para bater&iacute;as originales de f&aacute;brica.</p>
<p><strong style="">2. SUPERFICIES CROMADAS, ALUMINIO</strong></p>
<p>Cobertura: 6 meses sin l&iacute;mite de kilometraje.</p>
<p>La cobertura de garant&iacute;a incluye todos los componentes con cromo, aluminio pulido o superficies de Acero inoxidable.</p>
<p><strong>2.1. EXCLUSIONES</strong></p>
<p>Corrosi&oacute;n&nbsp; y manchas causadas por agentes corrosivos, qu&iacute;micos o m&eacute;todos inapropiados de limpieza o mantenimiento.</p>
<p>&nbsp;3.&nbsp;<strong style="">PINTURA</strong></p>
<p>Cobertura: 12 meses &oacute; 160.000 Kms</p>
<p>La &nbsp;garant&iacute;a incluye todas las superficies&nbsp; externas de la cabina, excepto los incluidos en el chasis.</p>
<p><strong>3.1 &nbsp;EXCLUSIONES</strong></p>
<p>Pintura al interior del cap&oacute;.</p>
<p>Pintura al interior de guardabarros.</p>
<p>Pintura en la superficie trasera de la cabina.</p>
<p>&nbsp;4.&nbsp;<strong style="">CORROSION</strong></p>
<p>L&iacute;mite de garant&iacute;a: 6 meses sin l&iacute;mite de kilometraje.</p>
<p>La cobertura ofrece garant&iacute;a contra la corrosi&oacute;n de cualquier metal o aleaci&oacute;n met&aacute;lica que forme parte del veh&iacute;culo.</p>
<p><strong>&nbsp;</strong><strong>4.1. EXCLUSIONES</strong></p>
<p>Corrosiones graves causadas por utilizaci&oacute;n de soluciones inadecuadas en el lavado, alta presi&oacute;n, detergentes, etc.</p>
<p>Corrosiones causadas por sales corrosivas de carretera.</p>
<p>Da&ntilde;os ambientales incluidos los del oc&eacute;ano u otros actos de la naturaleza.</p>
<p>Corrosiones generadas por modificaciones en la cabina.</p>
<h3>PROCEDIMIENTO PARA HACER EFECTIVA LA &nbsp;GARANTIA</h3>
<p>El Propietario podr&aacute; solicitar que se haga efectiva la garant&iacute;a en cualquiera de los talleres o concesionarios autorizados en el territorio colombiano publicados en nuestra p&aacute;gina web <a href="http://www.daimlerchrysler.com.co">www.daimler.com.co</a>.</p>
<h3>RESPONSABILIDAD</h3>
<p>Daimler Colombia S.A. y las plantas fabricantes de los veh&iacute;culos vendidos por la &nbsp;Compa&ntilde;&iacute;a o por sus concesionarios autorizados del pa&iacute;s, se reservan el derecho de modificar y/o mejorar en cualquier momento&nbsp; las especificaciones de sus productos y el cubrimiento de la garant&iacute;a, sin que por ello contraigan la obligaci&oacute;n de efectuar tales modificaciones o perfeccionamientos en sus productos vendidos anteriormente.</p>
<p>&nbsp;Daimler Colombia S.A. tiene un mecanismo institucional de recepci&oacute;n y tr&aacute;mite de Peticiones, Quejas y Reclamos (PQR), si usted lo requiere puede acudir al Coordinador de Customer Care, quien le explicara nuestro procedimiento interno de PQR. Los datos de contacto son: Coordinador de Customer Care, tel. 5187474 ext. 1642 y/o a los tel&eacute;fonos (1) 742 6850 y el celular es 3162801318 de Bogot&aacute;. La presentaci&oacute;n de PQR&acute;S No tiene que ser personal, ni requiere de intervenci&oacute;n de abogado.</p>
<p>En caso de persistir la inconformidad, le recordamos que puede acudir ante las autoridades competentes</p>
<p>Declaro que he recibido, le&iacute;do completamente, comprendido y aceptado los&nbsp; t&eacute;rminos, las condiciones de la garant&iacute;a y las dem&aacute;s informaciones contenidas en &eacute;ste documento.</p>
   </div>
  </div>

            <?php 
                include "include/lateral-clientes.php";  
            ?>
        
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al Inicio">Home</a><span class="sep">></span>
        <a >Clientes</a> <span class="sep">></span>
        <a href="freightliner.php" title="Ir a Freightliner">Freightliner</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>      