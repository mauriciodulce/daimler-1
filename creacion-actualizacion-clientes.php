<?php 
$cliente = "datos";
include "include/header.php";

?>
<!--BANNER INICIO-->
<div class="tituloheader"><h1> Actualización de clientes</h1></div>
<div class="cabecera datos" style="background-image: url('img/clientes/internet.jpg')"></div>

<!--CONTENIDOS-->
<div class="container content-interna">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 ">
            <div class="cap1">
                <h3>Estimados clientes</h3>
                <p>De conformidad con lo establecido en el Código de Integridad del Grupo Daimler, vigente desde el 1 de Noviembre de 2012, ha sido un compromiso de la Organización el aseguramiento de una conducta ética de negocios. Es así como el Código de Integridad  expedido el 1 de Noviembre de 2012, señala expresamente:
En nuestras actividades en todo el mundo y en la apertura a nuevos mercados cumplimos asimismo con otras leyes y regulaciones tales como (…):</p>

                <p>
                    <strong>Las leyes contra el lavado de dinero,<br>Las leyes antiterrorismo.</strong>
                </p>
                <p>
                    Con fundamento en lo anterior y en desarrollo de la Política Corporativa C 190.0, vigente a partir del 1 de Agosto de 2014 y la Resolución 100-000005 expedida por la Superintendencia de Sociedades, la Junta Directiva de Daimler Colombia S.A. a través del acta No 47 del 12 de Diciembre de 2014 ha adoptado el Manual de prevención y control del riesgo de Lavado de activos y Financiación del terrorismo (LA/FT), el cual es obligatoria observancia para todos los empleados, socios de negocios, accionistas, proveedores y clientes de Daimler Colombia S.A.
De igual manera,  Daimler Colombia S.A. ha designado a Carolina Rojas López (Gerente de Compliance) y su suplente Nubia Damian (Jefe de Impuestos y Análisis Contable) como los funcionarios responsables por velar por la implementación y cumplimiento de las medidas que hacen parte del Programa de prevención y control de este riesgo, por lo que en caso de requerir información de nuestro programa, por favor contacte directamente a estos dos funcionarios: carolina.rojas@daimler.com  Tel: 5187474 Ext 1120 y nubia.damian@daimler.com Tel: 5187474 Ext 1250.
</p><p>Por favor consulte el Manual de Prevención y Control del Riesgo de LA/FT. Y como cliente diligencie el Formato de conocimiento del cliente y envíe físicamente la documentación soporte requerida en el mismo a nuestra oficina principal Cra 7 #120-20, Centro Empresarial Usaquén Plaza, dirigido a nombre de su asesor comercial (de vehículos, repuestos o servicio).
Finalmente, les agradecemos su apoyo en el decidido compromiso de Daimler de prevenir el riesgo de lavado de activos, financiación del terrorismo y corrupción, pues este tipo de riesgos representan una tarea conjunta.
                </p>
            </div>
            <div class="descargas">
                <h3>Descargas</h3>
                <div>
                    <div class="imgPdf"></div>
                    <div class="descr">
                        <a href="/manual.pdf" target="blank">Manual de prevención y control de riesgo de LA y FT</a><br>
                        <span class="subt">PDF (400 KB)</span>
                    </div>
                </div>
                <div>
                    <div class="imgDown"></div>
                    <div class="descr">
                        <a href="formato.zip">Formato de conocimiento del cliente</a><br>
                        <span class="subt">PDF (59.2 KB)</span>
                    </div>
                </div>
            </div>

        </div>
            <?php 
                include "include/lateral-clientes.php";  
            ?>
          
    </div>
</div>
<!-- MIGA-->

<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al Inicio">Home</a><span class="sep">></span>
        <a href="#" title="Ir a Home">Clientes</a> <span class="sep">></span>
        <a href="#" title="Ir a Creación y actualización de datos">Creación y actualización de datos</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>   
