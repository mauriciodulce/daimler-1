<?php 
include "include/header.php";

?>
<div class="inner home">  
   
<!--slider-->
    <section id="slider">
        <div class="owl-carousel carouselprincipal">

                <div class="item"  data-dot="pruebas" style="background-image:url('img/slide/11.jpg')">
                   <div class="capsulaSlider" >
                       <a class="fancybox fancybox.iframe" href="video.html"> <div class='contblack'><div class="arrow"></div>Bienvenido a Daimler Colombia</a></div>
                    </div>
 
                </div>
                <div class="item"  data-dot="pruebas" style="background-image:url('img/slide/2.jpg')">
                    <div class="capsulaSlider" >
                        
                        <a href="<?php echo $path ?>mercedes-benz.php"> <div class='contblack'><div class="arrow arrow2"></div>Conoce todo lo que Mercedes-Benz<br> tiene para ti</a></div>
                    </div>
                </div>
                <div class="item"  data-dot="pruebas" style="background-image:url('img/slide/3.jpg')">
                    <div class="capsulaSlider" >
                        
                        <a href="<?php echo $path ?>freightliner.php"> <div class='contblack'><div class="arrow"></div>Freightliner</a></div>
                    </div>
                </div>

                <div class="item"  data-dot="pruebas" style="background-image:url('img/slide/5.jpg')">
                    <div class="capsulaSlider" >
                        
                        <a href="<?php echo $path ?>fuso.php"> <div class='contblack'><div class="arrow"></div>FUSO</a></div>
                    </div>
                </div>


                <!--<div class="item"  data-dot="pruebas" style="background-image:url('img/slide/4.jpg')">
                   <div class="capsulaSlider" >
                        
                        <a href="<?php echo $path ?>ciudadanodaimler"> <div class='contblack'><div class="arrow"></div>Ciudadano Daimler</a></div>
                    </div>
                </div>-->

        </div>
        <div class="pieSlider">
            <div class="capsulaSlider contenedorDots">
                <div class="dotsCont">
                     <div class="dot"><span>Bienvenido a Daimler</span></div>
                     <div class="dot"><span>Mercedes-Benz</span></div>
                     <div class="dot"><span>Freightliner</span></div>
                     <div class="dot"><span>FUSO</span></div>
                     <!--<div class="dot"><span>Ciudadano Daimler</span></div>-->
                 </div>
            </div>
        </div>  
    </section>
</div>
<script>
           $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                items:1,
                margin:0,
                nav:true,
                callbacks:true,
                navText:[],
                dotsContainer:'.dotsCont',
                center:true,
                smartSpeed:450,
                //loop:loop,
                //autoplay:true,
                autoplayHoverPause:true
            });
          });
         
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});

    </script>

<?php 
include "include/destacados.php";
include "include/footer.php";
?> 