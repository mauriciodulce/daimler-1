<?php 
include "include/header.php";
$cliente="mercedes";
?>
<div class="inner clientes container">
     <div class="tituloheader"><h1>Privacidad</h1></div>
     <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-3 col-lg-8 col-lg-offset-3 terminos">
         
            <h2>Protecci&oacute;n de datos</h2>
            <p>Daimler Colombia S.A. le agradece su visita a nuestra p&aacute;gina web, as&iacute; como su inter&eacute;s por nuestra empresa y nuestros productos. Nos tomamos muy en serio la protecci&oacute;n de sus datos personales y deseamos que se sienta a gusto durante la visita a nuestra p&aacute;gina web. La protecci&oacute;n de su &aacute;mbito privado durante el procesamiento de datos personales es uno de nuestros objetivos m&aacute;s importantes. Los datos personales que obtenemos cuando se visitan nuestras p&aacute;ginas web son procesados de acuerdo con lo establecido en la Ley 1581 de 2012 y las dem&aacute;s disposiciones legales vigentes sobre protecci&oacute;n de datos. Por lo dem&aacute;s, nuestra pol&iacute;tica de protecci&oacute;n de datos se rige por el c&oacute;digo de conducta vigente del Grupo Daimler y el Manual Interno de Pol&iacute;ticas y de Procedimientos para garantizar el tratamiento de los datos personales de Daimler Colombia S.A. (en adelante el &quot;Manual interno&quot;). Las p&aacute;ginas web de Daimler pueden contener enlaces a p&aacute;ginas web de otros proveedores, las cuales quedan excluidas de la presente declaraci&oacute;n de protecci&oacute;n de datos.</p>
            <ul type="disc">
                <li><a href="manual-proteccion-datos.pdf" target="_blank">Manual Interno de Pol&iacute;ticas y de Procedimientos para garantizar el tratamiento de los datos personales de Daimler Colombia S.A.</a></li>
              <li><a href="directriz-proteccion-datos.pdf" target="_blank">Directriz de protecci&oacute;n de datos, Daimler AG. </a></li>
            </ul>
         
            <h2>Obtenci&oacute;n y procesamiento de datos personales</h2>
            <p>Al visitar nuestras p&aacute;ginas web, nuestros servidores web almacenan, la siguiente informaci&oacute;n: </p>
            <ul type="disc">
              <li>La direcci&oacute;n IP que le ha sido asignada por su proveedor de servicios de Internet.</li>
              <li>La p&aacute;gina web desde la que nos visita.</li>
              <li>Nuestras p&aacute;ginas visitadas, as&iacute; como la fecha y la duraci&oacute;n de la visita.</li>
              <li>Los dem&aacute;s datos personales s&oacute;lo ser&aacute;n almacenados en cumplimiento de la legislaci&oacute;n local y el manual interno, en caso de ser facilitados por usted, por ejemplo, con motivo de una inscripci&oacute;n, una encuesta, un concurso, o para la celebraci&oacute;n de un contrato, y siempre con su consentimiento previo y expreso.</li>
            </ul>
            
            <h2>Finalidad, uso y transmisi&oacute;n de sus datos personales.</h2>
            <p>Siempre que medie previa y expresa autorizaci&oacute;n para el efecto y en la medida de lo necesario, en concordancia con la legislaci&oacute;n local y el manual interno, Daimler Colombia S.A. utilizar&aacute; sus datos personales para la administraci&oacute;n t&eacute;cnica de las p&aacute;ginas web, la gesti&oacute;n de clientes, la realizaci&oacute;n de encuestas de productos y tareas de marketing, siempre .</p>
            <p>Solo en el caso que disposiciones legales de &aacute;mbito nacional as&iacute; lo exijan, se transmitir&aacute;n datos personales a organismos y entidades gubernamentales. Hemos impuesto a las dem&aacute;s Compa&ntilde;&iacute;as pertenecientes a nuestro Grupo, a nuestros trabajadores, distribuidores, agencias encargadas del tratamiento de datos y concesionarios, el deber de conservar la confidencialidad de este tipo de informaci&oacute;n en caso que usted nos autorice a compartir la informaci&oacute;n con los mismos.</p>
         
            <h2>Posibilidad de elecci&oacute;n</h2>
            <p>Una de las finalidades de la recolecci&oacute;n de sus datos es poderlos utilizar para informarle acerca de nuestros productos y servicios ypara hacerle algunas preguntas respecto a los mismos. Por supuesto, la participaci&oacute;n en actividades de este tipo es voluntaria. En caso de que no est&eacute; de acuerdo con lo anterior, puede comunic&aacute;rnoslo en cualquier momento para que podamos bloquear los datos de manera oportuna. A trav&eacute;s del correo electr&oacute;nico: <strong>datospersonalesdco@daimler.com</strong></p>
            
            
            
            
            

            
            
            
            <h2>Cookies</h2>
            <p>Daimler Colombia  S.A. utiliza cookies para poder conocer las preferencias de los visitantes y  as&iacute; poder optimizar la presentaci&oacute;n de las p&aacute;ginas web. Las cookies son  peque&ntilde;os archivos que se almacenan temporalmente en su disco duro. La  informaci&oacute;n contenida en las cookies facilita la navegaci&oacute;n y hace posible que  el usuario alcance un alto grado de satisfacci&oacute;n al visitar una p&aacute;gina web.  Asimismo, las cookies nos ayudan a identificar las secciones m&aacute;s populares de  nuestra oferta en Internet. De esta forma podemos adaptar a sus necesidades los  contenidos de nuestra p&aacute;gina web de un modo m&aacute;s preciso y, por tanto, mejorar  nuestras ofertas para usted. Las cookies tambi&eacute;n resultan útiles a la hora de  averiguar si ya se ha producido previamente una comunicaci&oacute;n entre su ordenador  y nuestra p&aacute;gina. Se identifica únicamente el cookie en su ordenador. Los datos  relacionados con su persona s&oacute;lo se almacenar&aacute;n en forma de cookies si usted lo  permite (por ejemplo, para simplificar el acceso a un sitio protegido en  Internet, de tal forma que usted no deba introducir su ID de usuario y su  contrase&ntilde;a cada vez que entra.)</p>
            <p>Naturalmente,  tambi&eacute;n puede visitar nuestra p&aacute;gina web sin cookies. La mayor&iacute;a de los  browsers aceptan cookies de forma autom&aacute;tica. Usted podr&aacute; evitar el  almacenamiento de cookies en su disco duro mediante la elecci&oacute;n en su  configuraci&oacute;n del browser del comando *No aceptar cookies*. Podr&aacute; obtener  informaci&oacute;n m&aacute;s detallada de las instrucciones de su fabricante de browsers.  Puede eliminar las cookies que ya est&eacute;n almacenadas en su ordenador en  cualquier momento. El hecho de no aceptar cookies podr&aacute; suponer una limitaci&oacute;n  de las funciones de nuestras ofertas.</p>
            <h2>Seguridad</h2>
            <p>Daimler Colombia  S.A. utiliza medidas de seguridad t&eacute;cnicas y organizativas para proteger sus  datos tratados por nosotros frente a manipulaciones accidentales o dolosas,  p&eacute;rdidas, destrucciones o el acceso de terceros no autorizados. Nuestras  medidas de seguridad son permanentemente actualizadas de acuerdo con el  desarrollo tecnol&oacute;gico.</p>
            <h2>Derecho de informaci&oacute;n</h2>
            <p>Tras la correspondiente solicitud,  Daimler Colombia S.A. o el distribuidor de su pa&iacute;s le comunicar&aacute; inmediatamente  por escrito, de conformidad con la legislaci&oacute;n aplicable, si tenemos  almacenados datos personales suyos, y cu&aacute;les son &eacute;stos. En caso de que figure  registrado como usuario, le ofrecemos la posibilidad de consultar personalmente  sus datos, y, en su caso, proceder a su eliminaci&oacute;n o modificaci&oacute;n. Cuando, a  pesar de nuestros esfuerzos por disponer de datos correctos y actualizados se  hayan almacenado datos err&oacute;neos, procederemos a su correcci&oacute;n tras la oportuna  solicitud.<br />
              <br />
              En caso de que tenga preguntas acerca del tratamiento de sus datos personales,  puede dirigirse al responsable de la protecci&oacute;n de datos de nuestro grupo,  quien junto con su equipo, est&aacute; a su disposici&oacute;n en caso de que necesite  informaci&oacute;n, desee hacer sugerencias o plantear quejas.<br />
              <br />
              Responsable de la protecci&oacute;n de datos del grupo <br />
              Chief officer corporate data protection<br />
              <br />
              Doctor Joachim Riess<br />
              Daimler AG<br />
              HPC 0624<br />
              D-70546 Stuttgart<br />
              Alemania</p>
            <p>Responsable local de protecci&oacute;n de datos<br /><br />
                Juan Pablo Manotas Figueroa<br/>
                Daimler Colombia S.A.<br/>
                Cra 7 #120-20, Centro Empresarial Usaquén Plaza<br/>
                Bogot&aacute;, Colombia<br/>
                Email: datospersonalesdco@daimler.com<br/>
            </p>
            
            <h2>Habeas Data</h2>
            
     <p class="textogeneral">Daimler Colombia S.A. cuentan en su base de datos con información previamente suministrada por Usted, la cual ha sido recolectada por relaciones comerciales y/o laborales realizadas en desarrollo de nuestro objeto social.  
A raíz de la entrada en vigencia de la Ley Estatutaria 1581 del 2012, requerimos su autorización para el tratamiento de sus datos personales, según lo dispone el artículo 9° de la misma ley, a fin de ser incorporados a las bases de datos de Daimler Colombia S.A. 
Si Usted desea continuar recibiendo nuestra información, le confirmamos que puede ejercer sus derechos a conocer, actualizar, rectificar y solicitar la supresión de sus datos personales en cualquier momento, acorde con lo dispuesto en las mencionadas normas. 
De aceptar la presente opción, sus datos personales serán incluidos en nuestra base de datos, los cuales serán utilizados en los t&eacute;rminos dados en la autorizaci&oacute;n por el titular del dato y para los fines previstos en el Manual Interno.
</p>



           <p class="textogeneral">Si por el contrario, Usted no se encuentra interesado en recibir información de Daimler Colombia S.A., el Grupo Daimler y/o de sus colaboradores, le solicitamos comunicarlo a los siguientes correos electr&oacute;nicos: 
            <strong>datospersonalesdco@daimler.com</strong>
          </p>
                
           <p class="textogeneral">Finalmente, Daimler Colombia S.A. le informa que cuenta con políticas corporativas para el manejo de datos personales, las cuales puede consultar aqu&iacute;.</p>
        </div>
     </div><!-- .inner clientes-->
     
<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al inicio">Home</a><span class="sep">></span>
        <a href="privacidad.php" title="Ir a Privacidad">Privacidad</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>   