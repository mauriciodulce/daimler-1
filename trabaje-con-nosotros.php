<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include "include/header.php";
require 'vendor/autoload.php';

$mail = new PHPMailer(true);

$message1="";

if(isset($_POST['submit']))
{
    try {
        $c_name=$_POST['nombre']; // Contain Candidate name
        $email_id =$_POST['email']; //Contain candidate e-mail id
        $mob_number =$_POST['telefono']; //Contain candidate mobile number
        $ciudad =$_POST['ciudad'];
        $mensaje =$_POST['mensaje'];

        $message="";
        $message .="    Se ha recibido una nueva postulación en la sección 'trabaje con nosotros' en la página web de Daimler<br><br>
        <table width='800' border='1' cellspacing='0' cellpadding='8' bordercolor='#CCCCCC'>      
            <tr>        
                  <td colspan='2' bgcolor='#CDD9F5'><strong>Detalles del candidato</strong></td>               
            </tr> 
            <tr>        
                <td width='168' bgcolor='#FFFFEC'><strong>Nombre</strong></td>        
                <td width='290' bgcolor='#FFFFEC'>$c_name</td>      
            </tr>      
            <tr>        
                <td bgcolor='#FFFFDD'><strong>E-mail </strong></td>        
                <td bgcolor='#FFFFDD'>$email_id</td>      
            </tr>
            <tr>        
                <td bgcolor='#FFFFDD'><strong>Teléfono</strong></td>        
                <td bgcolor='#FFFFDD'>$mob_number</td>      
            </tr>
            <tr>        
                <td bgcolor='#FFFFDD'><strong>Ciudad</strong></td>        
                <td bgcolor='#FFFFDD'>$ciudad</td>      
            </tr>
            <tr>        
                <td bgcolor='#FFFFDD'><strong>Mensaje</strong></td>        
                <td bgcolor='#FFFFDD'>$mensaje</td>      
            </tr>
         </table>";
        $subject  ="Nueva hoja de vida en sitio web"; //like--- Resume From Website
        /*$headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: Contacto daimler <no-reply@daimler.com.co>' . "\r\n";
        mail('christian.lesmes@nabica.com.co', $subject, $message,$headers);*/
        $headers  ="";
//            include("PHPMailer/PHPMailerAutoload.php"); //Here magic Begen we include PHPMailer Library.
//            include("PHPMailer/class.phpmailer.php");
//            include("include/PHPMailer-master/PHPMailerAutoload.php"); //Here magic Begen we include PHPMailer Library.

        // Enable verbose debug output
        //Server settings
        $mail->CharSet = 'UTF-8';
        //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'daimlerweb@daimler.com.co';                 // SMTP username
        $mail->Password = 'D@iml3rW3b';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom('noreply@daimler.com.co', 'Nueva hoja de vida en sitio web'); //You Can add your own From mail
        $mail->addAddress('fabian.cabezas@nabica.com.co'); // Add a recipient id where you want to send mail

        $mail->addAttachment($_FILES['cv']['tmp_name'],$_FILES['cv']['name']); //This line Use to Keep User Txt,Doc,pdf file ,attachment
        $mail->addReplyTo($email_id); //where you want reply from user
        $mail->isHTML(true);
        $mail->Subject=''.$subject;
        $mail->Body=''.$message;
        $mail->send();
        $mensajeFinal="La solicitud no se ha podido enviar, intenta de nuevo.";
    }catch (Exception $e) {
        $mensajeFinal="La solicitud ha sido enviada.";
    }

}
else
{
    $message1.= "Code Error";
}
?>



<style>
    [type="checkbox"]:not(:checked),
    [type="checkbox"]:checked {
        position: absolute;
        left: -9999px;
    }
    [type="checkbox"]:not(:checked) + label,
    [type="checkbox"]:checked + label {
        position: relative;
        padding-left: 25px;
        cursor: pointer;
    }

    [type="checkbox"]:not(:checked) + label:before,
    [type="checkbox"]:checked + label:before {
        content: '';
        position: absolute;
        left:0; top: 1px;
        width: 20px; height: 20px;
        border: 1px solid #aaa;
        background: #f8f8f8;
        border-radius: 3px;
    }

    [type="checkbox"]:not(:checked) + label:after,
    [type="checkbox"]:checked + label:after {
        content: '\2713';
        position: absolute;
        top: 3px; left: 4px;
        font-size: 18px;
        line-height: 0.8;
        color: darkcyan;
        transition: all .2s;
    }
    [type="checkbox"]:not(:checked) + label:after {
        opacity: 0;
        transform: scale(0);
    }
    [type="checkbox"]:checked + label:after {
        opacity: 1;
        transform: scale(1);
    }
</style>
<div class="inner clientes">
    <div class="tituloheader"><h1> Trabaje con nosotros</h1></div>
    <!--BANNER INICIO-->
    <div class="cabecera mercedes" style="background-image: url('img/contacto.jpg')">
    </div>

    <!--CONTENIDOS-->
    <div class="container content-interna">
        <div class="row">
            <div class="col-md-3">

                <p>
                    En Daimler buscamos gente apasionada, exigente con los objetivos que se propone, integra y comprometida con la excelencia.
                    <br>
                    <strong> ¡The best or nothing!</strong>
                </p>

            </div>
            <div class="col-md-8 ">
                <h3 class="margin0"></h3>

                <div class="col-md-10">
                    <div class="row">
                        <?php  if(isset($_POST['submit'])){ echo $mensajeFinal; }  ?>
                        <form accept-charset="utf-8" id="formContacto" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input  type="text" name="nombre" id="nombre" class="form-control input-lg" placeholder="Nombre completo" required/>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Correo electrónico" required/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="ciudad" id="ciudad" class="form-control input-lg" placeholder="Ciudad" required/>
                            </div>
                            <div class="form-group">
                                <input  type="text" name="telefono" id="telefono" class="form-control input-lg" placeholder="Teléfono" required/>
                            </div>
                            <div class="form-group">
                                <textarea name="mensaje" class="form-control input-lg" rows="3" placeholder="Mensaje"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="file" name="cv" id="hojaVida" class="inputfile" required="" data-multiple-caption="{count} files selected" multiple/>
                                <label for="hojaVida" class="labelFile"><img src="img/icon-small-down.png" alt=""/> <span>Adjuntar hoja de vida</span></label>
                            </div>

                            <div class="form-group text-left">
                                <input type="checkbox" class="form-control" name="conf" id="conf"/>
                                <label for="conf" style='font-weight:normal;'>Acepto haber le&iacute;do y comprendido el MANUAL INTERNO DE POL&Iacute;TICAS Y DE PROCEDIMIENTOS PARA GARANTIZAR EL TRATAMIENTO DE LOS DATOS PERSONALES de DAIMLER COLOMBIA S.A. y por lo tanto, acepto libremente el tratamiento que se le dar&aacute; a mis datos transferir dichos a las personas naturales o jur&iacute;dicas de acuerdo con las finalidades y condiciones mencionadas en dicho Manual y en la Pol&iacute;tica de Protecci&oacute;n de Datos Personales</label>
                            </div>
                            <div id="check_msg" style="color:#FF0000"></div>

                            <button type="submit" name="submit" class="btn btn-negro btnContact input-lg">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al inicio">Home</a><span class="sep">></span>

        <a href="trabaje-con-nosotros.php" title="Ir a Trabaje con nostros">Trabaje con nosotros</a>
    </div>
</div>
<script>
    var inputs = document.querySelectorAll( '.inputfile' );
    Array.prototype.forEach.call( inputs, function( input )
    {
        var label	 = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                label.querySelector( 'span' ).innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });
</script>
<?php
include "include/destacados.php";
include "include/footer.php";
?>