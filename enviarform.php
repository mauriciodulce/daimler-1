<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require 'vendor/autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    $cliente="mercedes";
    /****INICIO CORREO********/
    //recibir los datos
    extract($_POST);
    $subject = 'Contacto Daimler';
    $message = '
    Contacto recibido!<br><br>
    Se ha enviado un mensaje de contacto desde la página de Daimler <br><br>'
    .'Nombre: '.$nombre.'<br>'
    .'Correo electrónico: '.$email.'<br>'
    .'Ciudad: '.$ciudad.'<br>'
    .'Teléfono: '.$telefono.'<br>'
    .'Mensae: '.$mensaje.'<br>';
    
    //Server settings
    $mail->CharSet = 'UTF-8';
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'daimlerweb@daimler.com.co';                 // SMTP username
    $mail->Password = 'D@iml3rW3b';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('daimlerweb@daimler.com.co', 'no-reply');
    $mail->addAddress('info@daimler.com.co', 'info');
    $mail->addAddress('daimlerweb@daimler.com.co', 'daimlerweb');

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body    = $message;
    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
