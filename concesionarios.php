<?php 
    include "include/header.php";
    include "include/conexionRecall.php";
?>
<div class="inner clientes">
    <div class="tituloheader"><h1>Red de concesionarios</h1></div>
    <!--BANNER INICIO-->
    <div class="cabecera conce" style="background-image: url('img/concesionarios.jpg')"></div>
    <!--CONTENIDOS-->
    <div class="container content-interna">
        <div class="row">
            <!--MENU LATERAL -->
            <aside class="col-lg-3 col-md-3 col-sm-4 ">
                <nav class="menuLateral">
                    <ul>
                        <?php
                            foreach ($conexion->query('SELECT * from recall_ciudades') as $ciudad) {
                                echo '<li><a data-id="#'.$ciudad['id'].'">'.$ciudad['nombre'].'</a></li>';
                            } 
                        ?>
                    </ul>
                </nav>

            </aside>
                <div class="col-lg-9 col-md-9 col-sm-8 tabinner concesionarios">
                <?php
                    foreach ($conexion->query('SELECT * from recall_ciudades') as $ciudad) {
                        echo 
                        '<div id="'. $ciudad['id'] .'" class="';

                        if (trim(strtolower($ciudad['nombre']))=='bogotá') {
                            echo "show ";
                        }

                        echo 'contCon">
                            <div class="col-md-12">
                                <h3>'. $ciudad['nombre'] .'</h3>
                            </div>';

                            $cont = 0;
                            foreach ($conexion->query('SELECT con.* from recall_ciudades as ciu, recall_concesionarios as con where ciu.id = con.ciudad_id and ciu.id ='.$ciudad['id']) as $concesionario) {
                                $cont = $cont + 1;
                                if ($cont % 2 == 1){
                                    echo
                                    '<div class="row capConce">
                                    <div class="col-md-6">
                                        <b>'. $concesionario['nombre'] .'</b><br>
                                        '. $concesionario['descripcion'] .'
                                    </div>';
                                }else{
                                    echo
                                    '<div class="col-md-6">
                                        <strong>'. $concesionario['nombre'] .'</strong><br>
                                        '. $concesionario['descripcion'] .'
                                    </div>
                                    </div>';
                                }
                             }

                            if ($cont % 2 == 1){
                                echo '</div>';
                            }
                        echo '</div>';
                    }
                ?>
                </div> 
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al Inicio">Home</a><span class="sep">></span>
        <a href="concesionarios.php" title="Ir a Red de concesionarios">Red de concesionarios</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
$conexion = null;
?>