<!-- DESTACADOS -->
<section id="destacados" class="container">
    <div class="row">
        <div class="col-md-4 col-lg-4">
          <div class="capsulanew">
              <div class="image" style="background-image: url('imagenes/menuBlanco/logo_mercedes.jpg')"></div>
              <div class="caption">
                <div class="arrow"></div>
                <h3>¡Yo actúo con Integridad!</h3>
                <p>Discusión sobre la importancia de la integridad en su ambiente de trabajo</p>
              </div>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="capsulanew">
              <div class="image" style="background-image: url('imagenes/menuBlanco/fair_play.jpg')"></div>
              <div class="caption">
                <div class="arrow"></div>
                <h3>FairPlay</h3>
                <p>Como comportarse correctamente ante diferentes tipos de situaciones.</p>
              </div>
              <a href="fairplay"></a>
          </div>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="capsulanew">
              <div class="image" style="background-image: url('imagenes/menuBlanco/semaforo_logo.jpg')"></div>
              <div class="caption">
                  <div class="arrow"></div>
                <h3>Contacto BPO</h3>
                <p>Ayuda activamente  a proteger su empresa y los empleados.</p>
              </div>
              <a href="BPO"></a>
          </div>

        </div>
    </div>
</section><!--/.secc2-->