<?php 
include "include/header.php";
$cliente="mercedes";
?>
<div class="inner clientes">
     <div class="tituloheader"><h1> Compañía</h1></div>
    <!--BANNER INICIO-->
    <div class="cabecera mercedes" style="background-image: url('img/compania.jpg')">
    </div>

    <!--CONTENIDOS--> 
    <div class="container content-interna">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                <!--Mision-->
                <div class="row">
                    <div class="col-md-12">
                        <h2>Misión</h2>
                    </div>
                    <div class="col-md-7">
                        <img src="img/misioncompania.jpg" alt=""/>
                    </div>  
                    <div class="col-md-5">
                        <p>Nuestro mayor orgullo es ser reconocidos como pioneros en la industria automotriz. Estamos comprometidos en ofrecer soluciones y experiencias excepcionales de movilidad y servicio a nuestros Clientes. Somos reconocidos gracias a nuestro excelente equipo de trabajo y al notable valor que proporcionamos a nuestros clientes, la Red de Distribución y Accionistas. </p>
                    </div>
                </div>
                
                <!-- Vision-->
                <div class="row">
                     <div class="col-md-12">
                        <h2>Objetivos</h2>
                     </div>
                    <div class="col-md-12">
                       
                        <ul>
                            <li><strong>Crecimiento: </strong>Participación de mercado y ventas.</li>
                            <li><strong>Rentabilidad:</strong> EBIT de los negocios ROS.</li>
                            <li><strong>Cliente:</strong> Satisfacción del cliente, Satisfacción del Dealer, Satisfacción de - Daimler con el Dealer.</li>
                            <li><strong>Personal:</strong> Calidad del personal-LEAD, Satisfacción de empleados y compromiso.</li>

                        </ul>
                        
                    </div>  
                    
                </div>
                
                <!--Valores-->
                <div class="row">
                    <div class="col-md-12">
                        <h2>Valores</h2>
                    </div>
                    <div class="col-md-5">
                        <p>Nos sentimos  comprometidos con la excelencia. Con objeto de satisfacer este nivel de exigencia, queremos crear y asumir una cultura de excelencia. Esta cultura se basa en cuatro valores: pasión, respeto, integridad y disciplina. Estos valores crean un marco de orientación capaz de motivar y de fomentar el trabajo en equipo. En último término,  nuestros valores empresariales son la clave de un crecimiento rentable y de éxito en nuestras operaciones.</p>
                        
                    </div>  
                    <div class="col-md-7">
                        <img src="img/valorescompania.jpg" alt=""/>
                    </div>
                </div>
                
            </div>
            
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al inicio">Home</a><span class="sep">></span>
        <a href="compania.php" title="Ir a Compañía">Compañía</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>      