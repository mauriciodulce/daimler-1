<?php 
$cliente="mercedes-comercial";
include "include/header.php";
?>
<div class="inner clientes">
    <!--BANNER INICIO-->
    <div class="tituloheader"><h1>Mercedes-Benz Vehículos Comerciales</h1></div>
    <div class="cabecera mercedes" style="background-image: url('img/header_MB-comerciales.jpg')"></div>

    <!--CONTENIDOS--> 
    <div class="container content-interna">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 ">
                <div class="cap1">
                    <h3>TÉRMINOS DE COBERTURA</h3>
                    <!--<p>lorem ipsum dolor sit amet, consectetur adipiscing elit. phasellus quis lectus metus, at posuere neque. sed pharetra nibh eget orci convallis at posuere leo convallis. sed blandit augue vitae augue scelerisque bibendum. vivamus sit amet libero turpis, non venenatis urna. in blandit, odio convallis.</p>
                        <p>suscipit venenatis, ante ipsum cursus augue, et mollis nunc diam eget sapien. nulla facilisi. etiam feugiat imperdiet rhoncus. sed suscipit bibendum enim, sed volutpat tortor malesuada non. morbi fringilla dui non purus porttitor mattis. suspendisse quis vulputate risus. phasellus erat velit, sagittis sed varius.</p>
                    -->    
                </div>
                <div class="tablecont">
<h3>CONDICIONES GENERALES</h3>
<p>La garant&iacute;a otorgada por Daimler Colombia S.A cubre la reparaci&oacute;n o cambio de las piezas o componentes originales del veh&iacute;culo que presenten defectos de calidad o ensamble de f&aacute;brica, sin costo alguno para el propietario salvo el de los componentes que sufren desgaste o deterioro por el uso normal del veh&iacute;culo (Ver componentes no cubiertos).</p>
<h3>CONDICIONES ESPECIALES</h3>
<p>Daimler Colombia S.A. podr&aacute; sustituir o reparar los componentes o piezas reconocidas como defectuosas o mal ensambladas de f&aacute;brica en el veh&iacute;culo. Los gastos de mano de obra para la remoci&oacute;n e instalaci&oacute;n de los componentes, conjuntos o sistemas del veh&iacute;culo por garant&iacute;a, tambi&eacute;n ser&aacute;n cubiertos por la misma, siempre que tales trabajos se realicen en los talleres autorizados por Daimler Colombia S.A., de acuerdo con el listado del presente documento,&nbsp; que se actualizar&aacute; peri&oacute;dicamente en la p&aacute;gina web www.daimler.com.co.</p>
<p>El propietario del veh&iacute;culo debe acreditar la realizaci&oacute;n de los servicios de conservaci&oacute;n y mantenimiento, se&ntilde;alando los mismos en el Cuaderno de Mantenimiento&nbsp; e Instrucciones de Servicio que se aconseja sean realizados prioritariamente en un taller autorizado por Daimler Colombia S.A.</p>
<p>En las reparaciones de garant&iacute;a, por causas imputables a Daimler Colombia &nbsp;S.A. y/o a uno de los &nbsp;concesionarios autorizados, se interrumpir&aacute; autom&aacute;ticamente el plazo de la garant&iacute;a y dicho tiempo se computar&aacute; como prolongaci&oacute;n de la misma.</p>
<p>No est&aacute;n incluidas dentro de la garant&iacute;a las consecuencias indirectas de un posible defecto tales como las p&eacute;rdidas comerciales, indemnizaciones por el tiempo de inmovilizaci&oacute;n, lucro cesante, gasolina, tel&eacute;fonos, viajes, gastos por alquiler de un veh&iacute;culo, alojamientos, comidas, almacenaje, etc.</p>
<h3>EXCLUSIONES DE LA GARANTIA</h3>
<p>La garant&iacute;a perder&aacute;&nbsp; su validez cuando ocurra cualquiera de los siguientes casos:</p>
<p>Si no se realizan los servicios de conservaci&oacute;n, revisiones y mantenimiento obligatorios en los per&iacute;odos se&ntilde;alados en el Cuaderno de Mantenimiento entregado al propietario siempre y cuando el evento reportado por el cliente est&eacute; relacionado directa o indirectamente a la falta o calidad del mantenimiento realizado.</p>
<p>Cuando se han producido da&ntilde;os en el veh&iacute;culo por &nbsp;no seguir las observaciones t&eacute;cnicas de Daimler Colombia S.A. relacionadas con (a.) el uso o manejo inadecuado del veh&iacute;culo, o (b.) servicios de conservaci&oacute;n y mantenimiento preventivos inadecuados y/o no efectuados de acuerdo a las condiciones de uso equivalentes (normales, mixtas y dif&iacute;ciles), as&iacute; como por lo citado en el Cuaderno de Mantenimiento y el evento reportado est&eacute; relacionado con estas condiciones incumplidas.</p>
<p>Si los da&ntilde;os o mal funcionamiento del veh&iacute;culo se deben a uso &nbsp;inadecuado (ej. sobrepeso, sobrecupo, uso inadecuado en caminos irregulares y no pavimentados), accidentes, vandalismo, condiciones ambientales adversas, desastres naturales y en general por caso fortuito o fuerza mayor.</p>
<p>Si el veh&iacute;culo ha sido utilizado en condiciones y prop&oacute;sitos distintos a&nbsp; las normales para los cuales fue dise&ntilde;ado o no ha sido operado siguiendo las recomendaciones del Manual de Operaci&oacute;n y el Cuaderno de Mantenimiento &nbsp;entregados junto con el veh&iacute;culo y&nbsp; las dem&aacute;s instrucciones comunicadas por Daimler Colombia S.A.</p>
<p>Si la instalaci&oacute;n de carrocer&iacute;as, blindajes, equipos de volteo, furgones, tanques adicionales, accesorios, equipo electr&oacute;nico, &nbsp;alarmas u otros componentes afectan el buen funcionamiento u ocasionen da&ntilde;os en el veh&iacute;culo.</p>
<p>Si el veh&iacute;culo ha sido alterado, modificado o reparado por personas distintas al personal calificado de los talleres autorizados o si se instalan en el veh&iacute;culo, repuestos o accesorios no autorizados por Daimler Colombia S.A. y dicha alteraci&oacute;n tiene relaci&oacute;n directa con la falla reportada.</p>
<p>Si fuesen constatadas violaciones o alteraciones al veloc&iacute;metro, od&oacute;metro (cuenta kil&oacute;metros) &nbsp;o si se observara que el mismo ha permanecido fuera de funcionamiento con el fin de obtener el beneficio de garant&iacute;a.</p>
<p>Elementos o piezas que hayan sido intervenidas o reparadas por colisi&oacute;n, ya que estas estar&aacute;n cubiertas por la garant&iacute;a de piezas o reparaci&oacute;n. De igual forma no tendr&aacute;n garant&iacute;a los veh&iacute;culos declarados en p&eacute;rdida total o que sean salvamento.</p>
<p>Da&ntilde;os en tapices y revestimientos internos relacionados con desgaste por el uso normal de estas piezas, al igual que deformaciones, decoloraciones y manchas ocasionadas por excesiva exposici&oacute;n al sol despu&eacute;s de 12 meses o 20.000 Km (lo primero que ocurra).</p>
<p>Ajustes al veh&iacute;culo: estos trabajos por no involucrar cambio de piezas (entonaci&oacute;n, ajuste de suspensi&oacute;n, ajuste de frenos, etc) no son cubiertos por la garant&iacute;a, ya que se generan a consecuencia del uso normal del veh&iacute;culo, Los ruidos y rechinidos que no sean consecuencia de un da&ntilde;o o desajuste mec&aacute;nico tendr&aacute;n cobertura hasta los 12 meses o 20.000 km (lo primero que ocurra).</p>
<h3><strong>MANTENIMIENTO</strong></h3>
<p>La presente garant&iacute;a no cubre costos de mantenimiento de acuerdo a lo establecido en los manuales de operaci&oacute;n y conservaci&oacute;n del veh&iacute;culo, o&nbsp; fallas debidas al uso de combustible, lubricantes o refrigerantes que no cumplan con las especificaciones recomendadas por Daimler Colombia S.A.</p>
<p>La realizaci&oacute;n de los mantenimientos establecidos en el cuaderno de mantenimiento y el uso de combustible, aceite, lubricantes, y refrigerantes apropiados es &nbsp;responsabilidad&nbsp; del propietario.</p>
<h3>PERIODO DE COBERTURA</h3>
<p>Esta garant&iacute;a aplica para el primer propietario y los due&ntilde;os subsiguientes y estar&aacute; vigente durante el periodo o el recorrido mencionados a continuaci&oacute;n, contados desde la fecha de entrega del veh&iacute;culo al primer propietario (estipulada en este certificado), y&nbsp; terminar&aacute; cuando se cumpla el plazo o alcance el kilometraje indicado, lo primero que ocurra y aplica para los veh&iacute;culos nuevos, importados o ensamblados por <strong>Daimler Colombia S.A, </strong>vendidos por la compa&ntilde;&iacute;a o por sus concesionarios autorizados, matriculados y usados en Colombia.</p>
<p>&nbsp;</p>
<table class="table tabled tablebold">
<tbody>
<tr style="height: 49px;">
<td style="height: 49px;" colspan="5">VIGENCIA GARANT&Iacute;A COMERCIALES MERCEDES BENZ</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">MODELO</td>
<td style="height: 49px;" colspan="2">VIGENCIA GENERAL</td>
<td style="height: 49px;" colspan="2">TREN DE FUERZA</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">&nbsp;</td>
<td style="height: 49px;">Tiempo</td>
<td style="height: 49px;">Kilometraje</td>
<td style="height: 49px;">Tiempo</td>
<td style="height: 49px;">Kilometraje</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">OF, OH, O500</td>
<td style="height: 49px;">12 meses</td>
<td style="height: 49px;">100.000 Km</td>
<td style="height: 49px;">24 meses</td>
<td style="height: 49px;">200.000 Km</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">Sprinter 515</td>
<td style="height: 49px;">24 meses</td>
<td style="height: 49px;">150.000 Km</td>
<td style="height: 49px;">24 meses</td>
<td style="height: 49px;">200.000 Km</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">Sprinter 415 SUSI</td>
<td style="height: 49px;">12 meses</td>
<td style="height: 49px;">50.000 Km</td>
<td style="height: 49px;">12 meses</td>
<td style="height: 49px;">100.000 Km</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">Atego1726, LO, Vito</td>
<td style="height: 49px;">12 meses</td>
<td style="height: 49px;">50.000 Km</td>
<td style="height: 49px;">12 meses</td>
<td style="height: 49px;">100.000 Km</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">Actros, Axor, Zetros, Atego 813- 1017-1016</td>
<td style="height: 49px;" colspan="2">12 meses sin l&iacute;mite Km</td>
<td style="height: 49px;">36 meses</td>
<td style="height: 49px;">250.000 Km</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;" colspan="2">Unimog</td>
<td style="height: 49px;" colspan="3">12 meses sin l&iacute;mite de Km.&nbsp;</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Todas las intervenciones realizadas al amparo de la Garant&iacute;a Contractual est&aacute;n garantizadas hasta el vencimiento del per&iacute;odo original de garant&iacute;a del veh&iacute;culo (tiempo o kilometraje). Para las reparaciones realizadas a partir de la finalizaci&oacute;n del mes 12 o repuestos comprados por mostrador aplica la siguiente vigencia.</p>
<p>La garant&iacute;a de la bater&iacute;a es de 12 meses sin l&iacute;mite de kilometraje</p>
<h3>PIEZAS O COMPONENTES NO CUBIERTOS POR LA GARANTIA</h3>
<p>La presente garant&iacute;a excluye componentes y piezas que presenten desgaste normal o que sean de mantenimiento como: filtros (lubricaci&oacute;n, admisi&oacute;n de aire, combustible, sistema neum&aacute;tico y sistema de aire acondicionado y calefacci&oacute;n), l&iacute;quidos de llenado (lubricantes, combustible, refrigerante, l&iacute;quido de frenos, fluidos hidr&aacute;ulicos, fluidos del sistema de aire acondicionado, grasas), buj&iacute;as, correas, soportes de motor, embrague (disco y prensa), retenedores, guayas y cables, guardapolvos, r&oacute;tulas, bujes y terminales de la suspensi&oacute;n, amortiguadores, muelles, ballestas, fuelles neum&aacute;ticos, discos, bandas, campanas y pastillas de freno, fusibles, rel&eacute;s, bombillos, cables el&eacute;ctricos del trailer, plumillas, vidrios y herramienta. Las piezas o sistemas antes mencionados podr&aacute;n ser cubiertos en los casos que se verifique que un defecto de fabricaci&oacute;n o ensamble de la pieza es la causa del evento reportado por el cliente.</p>
<p>La garant&iacute;a tampoco cubre alineaci&oacute;n y balanceo de ruedas, Mano de obra, ni el mantenimiento o reajuste de las partes &nbsp;anteriormente mencionadas y otras que est&eacute;n dentro del mismo concepto de desgaste normal o de mantenimiento.</p>
<p>Los neum&aacute;ticos gozan de la garant&iacute;a que otorgan sus fabricantes acogi&eacute;ndose a las condiciones otorgadas por los mismos. En todo caso, &nbsp;las llantas estar&iacute;an exentas en caso que el da&ntilde;o sea consecuencia de golpes, pellizcamientos causados por golpes, desgastes prematuros por falta o fallas en la alineaci&oacute;n y el uso de una presi&oacute;n de inflado incorrecta. En caso de cualquier reclamaci&oacute;n por Garant&iacute;a, el concesionario de su preferencia est&aacute; a disposici&oacute;n para encaminar su solicitud al representante local de la marca de los neum&aacute;ticos de su veh&iacute;culo.</p>
<h3>PROCEDIMIENTO PARA HACER EFECTIVA LA GARANTIA</h3>
<p>El Propietario podr&aacute; solicitar que se haga efectiva la garant&iacute;a en cualquiera de los talleres o concesionarios autorizados en el territorio colombiano publicados en nuestra p&aacute;gina web <a href="http://www.daimlerchrysler.com.co">www.daimler.com.co</a>:</p>
<h3>RESPONSABILIDAD</h3>
<p>Daimler Colombia S.A. y las plantas fabricantes de los veh&iacute;culos vendidos por la &nbsp;Compa&ntilde;&iacute;a o por sus concesionarios autorizados del pa&iacute;s, se reservan el derecho de modificar y/o mejorar en cualquier momento&nbsp; las especificaciones de sus productos y el cubrimiento de la garant&iacute;a, sin que por ello contraigan la obligaci&oacute;n de efectuar tales modificaciones o perfeccionamientos en sus productos vendidos anteriormente.</p>
<p>&nbsp;Daimler Colombia S.A. tiene un mecanismo institucional de recepci&oacute;n y tr&aacute;mite de Peticiones, Quejas y Reclamos (PQR), si usted lo requiere puede acudir al Coordinador de Customer Care, quien le explicara nuestro procedimiento interno de PQR. Los datos de contacto son: Coordinador de Customer Care, tel. 5187474 ext. 1642 y/o a los tel&eacute;fonos (1) 742 6850 y el celular es 3162801318 de Bogot&aacute;. La presentaci&oacute;n de PQR&acute;S No tiene que ser personal, ni requiere de intervenci&oacute;n de abogado.</p>
<p>En caso de persistir la inconformidad, le recordamos que puede acudir ante las autoridades competentes</p>
<p>Declaro que he recibido, le&iacute;do completamente, comprendido y aceptado los&nbsp; t&eacute;rminos, las condiciones de la garant&iacute;a y las dem&aacute;s informaciones contenidas en &eacute;ste documento.</p>
<p>&nbsp;</p>

            
            </div>

            
        
        </div>
        <?php 
                include "include/lateral-clientes.php";  
            ?>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al inicio">Home</a><span class="sep">></span>
        <a >Clientes</a> <span class="sep">></span>
        <a href="mercedes-benz-comerciales.php" title="Ir a Mercedes-Benz Vehículos Comerciales">Mercedes-Benz Vehículos Comerciales</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>      