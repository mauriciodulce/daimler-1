// *** NOTE: Syntax errors while editing this file could potentially cause an outage for all proxy users in the said location
// *** Edit with caution, try and maintain the same syntax as given below
//


// Function Header Block
function FindProxyForURL(url,host)
{

// to see debug messages in javascript console set this to true
	var DEBUG=false;

  // Location specific Proxy bypass
  // MB Columbia

  if (	url.substring(0, 11) == "http://127."  ||
	url.substring(0, 12) == "https://127." ||
	url.substring(0, 16) == "http://localhost"  ||
	url.substring(0, 17) == "https://localhost" ||
	shExpMatch(host, "*.usfdc.corpintra.net") ||
	shExpMatch(host, "*.americas.isn.corpintra.net") ||
	shExpMatch(host, "*.americas.bg.corpintra.net") ||
	shExpMatch(host, "*.fs.corpintra.net") ||
  	shExpMatch(host, "*.corpintra.net") ||
  	shExpMatch(url, "*53.92.225.*") ||
	shExpMatch(url, "https://*.wp.corpintra.net*") ||
  	shExpMatch(url, "https://*.wp.corpshared.net*") 

// Add any more location specific bypass items here using the same syntax as above

)
      { return "DIRECT"; }

  // Proxy bypass for local networks - identify local networks and list them below in the same syntax
  // Remove unwanted networks from the list below
  // The below list is provided only as an example. Local networks for each location are to be specifically identified and this list below must be amended

  if ( isPlainHostName(host) ||
       isInNet(host, "53.92.216.0", "255.255.248.0")
) 
      { return "DIRECT"; }

  // Proxy bypass for non-FQDN hosts

  if (isPlainHostName(host))
      { return "DIRECT"; }


// Proxy for MB Columbia clients
  return "PROXY proxy-loc-mbusa.americas.svc.corpintra.net:3128";

// ** Sayed Abedin ABEDINS ITI-GS Proxy Team **

}