<?php 
$provee="creacion";
include "include/header.php";

?>
<div class="inner clientes">
    <div class="tituloheader"><h1>Creación y actualización de proveedores</h1></div>
    <!--BANNER INICIO-->
    <div class="cabecera mercedes" style="background-image: url('img/creacionproveedores.jpg')"></div> 

    <!--CONTENIDOS--> 
    <div class="container content-interna">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 ">
                <h3>Creación y/o actualización de proveedores</h3>
         
                <p>De conformidad con lo establecido en el Código de Integridad del Grupo Daimler, vigente desde el 1 de Noviembre de 2012, ha sido un compromiso de la organización el aseguramiento de una conducta ética de negocios. Es así como el Código de Integridad&nbsp; expedido el 1 de Noviembre de 2012, señala expresamente:<u></u></p>
              <p><em>En nuestras actividades en todo el mundo y en la apertura a nuevos mercados cumplimos asimismo con otras leyes y regulaciones tales como:</em></p>
              <ul>
                <li><em>Las leyes contra el lavado de dinero.</em></li>
                <li><u></u><em>Las leyes antiterrorismo.</em></li>
              </ul><br>
              <p>Con fundamento en lo anterior y en desarrollo de la Política Corporativa C 190.0, vigente a partir del 1 de Agosto de 2014 y la resolución 100-000005 expedida por la Superintendencia de Sociedades, la Junta Directiva de DAIMLER COLOMBIA S.A. a través del acta No 47 del 12 de Diciembre de 2014 ha adoptado el Manual de Prevención y Control del Riesgo de Lavado de Activos y Financiación del Terrorismo (LA/FT), el cual es obligatoria observancia para todos los empleados, socios de negocios, accionistas, proveedores y clientes de DAIMLER COLOMBIA S.A.</p>
              <p>De igual manera,&nbsp; DAIMLER COLOMBIA S.A. ha designado a Tatiana Ramírez (Responsable Compliance)&nbsp;y su suplente Nubia Damian (Jefe de Impuestos y Análisis Contable) como los funcionarios responsables por velar por la implementación y cumplimiento de las medidas que hacen parte del programa de prevención y control de este riesgo, por lo que en caso de requerir información de nuestro programa, por favor contacte directamente a estos dos funcionarios:&nbsp;<span lang="EN-US"><a href="mailto:tatiana.ramirez@daimler.com" target="_blank"><span lang="ES-CO">tatiana.ramirez@daimler.com</span></a></span><span lang="EN-US">&nbsp;</span>&nbsp;Tel: 5187474 Ext 1121 y&nbsp;<span lang="EN-US"><a href="mailto:nubia.damian@daimler.com" target="_blank"><span lang="ES-CO">nubia.damian@daimler.com</span></a></span><span lang="EN-US">&nbsp;</span>Tel: 5187474 Ext 1250.</p>
              <p>Por favor consulte el Manual de Prevención y Control del Riesgo de LA/FT. Y como proveedor diligencie el Formulario Único de Proveedores y envíe físicamente la documentación soporte requerida en el mismo a nuestra oficina principal Cra 7 #120-20, Centro Empresarial Usaquén Plaza, dirigido al departamento de compras.<br>
              </p>
              
              <!--descargas-->
                <div class="descargas">
                    <h3>Descargas</h3>
                    <div>
                        <div class="imgPdf"></div>
                        <div class="descr">
                            <a href="/manualproveedor.pdf" target="blank">Manual de prevención y control de riesgo de LA y FT</a><br>
                            <span class="subt">PDF (400 KB)</span>
                        </div>
                    </div>
                    <div>
                        <div class="imgDown"></div>
                        <div class="descr">
                            <a href="/formato-proveedor.zip" target="blank">Formato de registro de proponentes</a><br>
                            <span class="subt">ZIP (57.5 KB)</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- barra lateral -->
            <aside class="col-lg-4 col-md-4 col-sm-4">

                <div class="capsulanew">
                    <div class="image" style="background-image: url('img/proveedores.jpg')"></div>
                    <div class="caption">
                        <div class="arrow"></div>
                        <h3 class="min">Condiciones generales<br >ordenes de compra</h3>
                     
                    </div>
                    <a href="condiciones-generales.php"></a>
                </div>
            </aside>
        
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al inicio">Home</a><span class="sep">></span>
        <a>Proveedores</a> <span class="sep">></span>
        <a href="creacion-actualizacion-proveedores.php" title="Ir a Creación y/o actualización de proveedores">Creación y/o actualización de proveedores</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>      