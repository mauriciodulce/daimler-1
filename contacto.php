<?php 
include "include/header.php";
$cliente="mercedes";

?>
<style>
    
[type="checkbox"]:not(:checked),
[type="checkbox"]:checked {
  position: absolute;
  left: -9999px;
}
[type="checkbox"]:not(:checked) + label,
[type="checkbox"]:checked + label {
  position: relative;
  padding-left: 25px;
  cursor: pointer;
}

[type="checkbox"]:not(:checked) + label:before,
[type="checkbox"]:checked + label:before {
  content: '';
  position: absolute;
  left:0; top: 1px;
  width: 20px; height: 20px;
  border: 1px solid #aaa;
  background: #f8f8f8;
  border-radius: 3px;
}

[type="checkbox"]:not(:checked) + label:after,
[type="checkbox"]:checked + label:after {
  content: '\2713';
  position: absolute;
  top: 3px; left: 4px;
  font-size: 18px;
  line-height: 0.8;
  color: darkcyan;
  transition: all .2s;
}

[type="checkbox"]:not(:checked) + label:after {
  opacity: 0;
  transform: scale(0);
}
[type="checkbox"]:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
</style>
<div class="inner clientes">
     <div class="tituloheader"><h1> Contáctenos</h1></div>
    <!--BANNER INICIO-->
    <div class="cabecera mercedes" style="background-image: url('img/contacto.jpg')">
    </div>

    <!--CONTENIDOS--> 
    <div class="container content-interna">
        <div class="row">
            <div class="col-md-3">
                <p>
                <strong>Teléfono de servicio al cliente:</strong><br>
                Línea Nacional:<br>(57) 1 5187474<br>
                Línea de atención al cliente fija:<br>(57) 1 7426850<br>
                Celular :<br>(57) 3162801318<br>

              </p>
                
                <hr>
                <p>
                <strong>Dirección:</strong><br>
                Cra 7 #120-20<br>
                Centro Empresarial Usaquén Plaza
                </p>
                
            </div>
            <div class="col-md-8 ">
                <h3 class="margin0">Preguntas y sugerencias</h3>
                
                <div class="col-md-10">
                    <div class="row">
                        <form id="formContacto" method="post">
                            <div class="form-group">
                                <input  type="text" name="nombre" id="nombre" class="form-control input-lg" placeholder="Nombre completo" required />
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Correo electrónico" required />
                            </div>
                            <div class="form-group">
                                <input type="text" name="ciudad" id="ciudad" class="form-control input-lg" placeholder="Ciudad"  />
                            </div>
                            <div class="form-group">
                                <input  type="text" name="telefono" id="telefono" class="form-control input-lg" placeholder="Teléfono" />
                            </div>
                            <div class="form-group">
                                <textarea name="mensaje" id="mensaje" class="form-control input-lg" rows="3" required placeholder="Mensaje"></textarea>
                            </div>
                            <div class="form-group text-left">
                                <input type="checkbox" class="form-control" name="conf" id="conf" required />
                                <label for="conf" style='font-weight:normal;'>Acepto haber le&iacute;do y comprendido el MANUAL INTERNO DE POL&Iacute;TICAS Y DE PROCEDIMIENTOS PARA GARANTIZAR EL TRATAMIENTO DE LOS DATOS PERSONALES de DAIMLER COLOMBIA S.A. y por lo tanto, acepto libremente el tratamiento que se le dar&aacute; a mis datos transferir dichos a las personas naturales o jur&iacute;dicas de acuerdo con las finalidades y condiciones mencionadas en dicho Manual y en la Pol&iacute;tica de Protecci&oacute;n de Datos Personales</label>
                            </div>
                            
                            <div id="check_msg" style="color:#FF0000"></div> 
                            <button type="submit" name="submit" id="submit" class="btn btn-negro btnContact input-lg">Enviar</button>

                            
                        </form> 

                            <p id="message_success_form" style="text-align:center;">
                            <br>
                              Correo enviado satisfactoriamente
                            </p>
                            <p id="message_error_form"  style="text-align:center;">
                            <br>

                              Error al enviar correo electrónico, intente nuevamente
                            </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al inicio">Home</a><span class="sep">></span>
        <a >Clientes</a> <span class="sep">></span>
        <a href="contacto.php" title="Ir a Contacto">Contacto</a> 
    </div>
</div>

<script type="text/javascript">
$(document).ready(function () {
  $("#message_success_form").hide();
  $("#message_error_form").hide();
})
      // process the form
        $('#formContacto').submit(function(event) {
          $("#submit").val("ENVIANDO...");
          $("#submit").text("ENVIANDO...");
          
        var formData = {
          'nombre'       : $('#nombre').val(),
          'email'   : $('#email').val(),
          'ciudad'      : $('#ciudad').val(),
          'telefono'      : $('#telefono').val(),
          'mensaje'      : $('#mensaje').val()

        };

        // process the form
        $.ajax({
            type        : 'POST', 
            url         : 'enviarform.php', 
            data        : formData,
              success: function(msg){
              //if (msg=="si") {
          $("#submit").text("Enviar");

                $("#message_success_form").show();
              $("#message_error_form").hide();
              /*}else{
          $("#submit").text("Enviar");
                
                $("#message_success_form").hide();
                $("#message_error_form").show();
              }*/
              
              }
        })
        
        event.preventDefault();
    });

</script>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>
