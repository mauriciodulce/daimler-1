<?php 
$cliente="mercedes";
include "include/header.php";
?>
<div class="inner clientes">
    <!--BANNER INICIO-->
    <div class="tituloheader"><h1>Mercedes-Benz</h1></div>
    <div class="cabecera mercedes" style="background-image: url('img/header_MB.jpg')"></div>

    <!--CONTENIDOS--> 
    <div class="container content-interna">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 ">
                <div class="cap1">
                    <h3>Mercedes-Benz</h3>
                    <!--<p>lorem ipsum dolor sit amet, consectetur adipiscing elit. phasellus quis lectus metus, at posuere neque. sed pharetra nibh eget orci convallis at posuere leo convallis. sed blandit augue vitae augue scelerisque bibendum. vivamus sit amet libero turpis, non venenatis urna. in blandit, odio convallis.</p>
                        <p>suscipit venenatis, ante ipsum cursus augue, et mollis nunc diam eget sapien. nulla facilisi. etiam feugiat imperdiet rhoncus. sed suscipit bibendum enim, sed volutpat tortor malesuada non. morbi fringilla dui non purus porttitor mattis. suspendisse quis vulputate risus. phasellus erat velit, sagittis sed varius.</p>
                    -->    
                </div>
                <h3>TERMINOS DE COBERTURA CONDICIONES GENERALES</h3>
<p>La garant&iacute;a otorgada por Daimler Colombia S.A cubre la reparaci&oacute;n o cambio de las piezas o componentes originales del veh&iacute;culo que presenten defectos de calidad o ensamble de f&aacute;brica, sin costo alguno para el propietario salvo el de los componentes que sufren desgaste o deterioro por el uso normal del veh&iacute;culo. (Ver componentes no cubiertos).</p>
<h3>CONDICIONES ESPECIALES</h3>
<p>Daimler Colombia S.A. podr&aacute; sustituir o reparar los componentes o piezas reconocidas como defectuosas o mal ensambladas de f&aacute;brica en el veh&iacute;culo. Los gastos de mano de obra para la remoci&oacute;n e instalaci&oacute;n de los componentes, conjuntos o sistemas del veh&iacute;culo por garant&iacute;a, tambi&eacute;n ser&aacute;n cubiertos por la misma, siempre que tales trabajos se realicen en los talleres autorizados por Daimler Colombia S.A., de acuerdo con el listado del presente documento, que se actualizar&aacute; peri&oacute;dicamente en la p&aacute;gina web <a href="http://www.daimler.com.co/">www.daimler.com.co</a>.</p>
<p>El propietario del veh&iacute;culo debe acreditar la realizaci&oacute;n de los servicios de conservaci&oacute;n y mantenimiento, se&ntilde;alando los mismos en el Cuaderno de Mantenimiento&nbsp; e Instrucciones de Servicio que se aconseja sean realizados prioritariamente en un taller autorizado por Daimler Colombia S.A.</p>
<p>En las reparaciones de garant&iacute;a, por causas imputables a Daimler Colombia &nbsp;S.A. y/o a uno de los &nbsp;concesionarios autorizados, se interrumpir&aacute; autom&aacute;ticamente el plazo de la garant&iacute;a y dicho tiempo se computar&aacute; como prolongaci&oacute;n de la misma.</p>
<p>No est&aacute;n incluidas dentro de la garant&iacute;a las consecuencias indirectas de un posible defecto tales como las p&eacute;rdidas comerciales, indemnizaciones por el tiempo de inmovilizaci&oacute;n, lucro cesante, gasolina, tel&eacute;fonos, viajes, gastos por alquiler de un veh&iacute;culo, alojamientos, comidas, almacenaje, etc.</p>
<h3>EXCLUSIONES DE LA GARANTIA</h3>
<p>La garant&iacute;a perder&aacute;&nbsp; su validez cuando ocurra cualquiera de los siguientes casos:</p>
<p>Si no se realizan los servicios de conservaci&oacute;n, revisiones y mantenimiento obligatorios de acuerdo con el sistema de mantenimiento activo Assyst y Assyst plus, siempre y cuando el evento reportado por el cliente est&eacute; relacionado directa o indirectamente a la falta o calidad del mantenimiento realizado.</p>
<p>Cuando se han producido da&ntilde;os en el veh&iacute;culo por &nbsp;no seguir las observaciones t&eacute;cnicas de Daimler Colombia S.A. relacionadas con (a.) el uso o manejo inadecuado del veh&iacute;culo, o (b.) servicios de conservaci&oacute;n y mantenimiento preventivos inadecuados y/o no efectuados de acuerdo a las condiciones de uso equivalentes (normales, mixtas y dif&iacute;ciles), as&iacute; como por lo citado en Cuaderno &nbsp;de Mantenimiento y el evento reportado est&eacute; relacionado con estas condiciones incumplidas.</p>
<p>Si los da&ntilde;os o mal funcionamiento del veh&iacute;culo se deben a uso &nbsp;inadecuado (ej. sobrepeso, sobrecupo, uso inadecuado en caminos irregulares y no pavimentados), accidentes, vandalismo, condiciones ambientales adversas, desastres naturales y en general por caso fortuito o fuerza mayor.</p>
<p>Si el veh&iacute;culo ha sido utilizado para una aplicaci&oacute;n distinta para la cual fue dise&ntilde;ado (servicio p&uacute;blico/competencias) o no ha sido operado siguiendo las recomendaciones de las instrucciones de servicio y el Cuaderno de Mantenimiento entregados junto con el veh&iacute;culo y&nbsp; las dem&aacute;s instrucciones comunicadas por Daimler Colombia S.A.</p>
<p>Si la instalaci&oacute;n de carrocer&iacute;as, blindajes, furgones, tanques adicionales, accesorios, equipo electr&oacute;nico, &nbsp;alarmas u otros componentes afectan el buen funcionamiento u ocasionen da&ntilde;os en el veh&iacute;culo.</p>
<p>Si el veh&iacute;culo ha sido alterado, modificado o reparado por personas distintas al personal calificado de los talleres autorizados por Daimler Colombia S.A. y dicha alteraci&oacute;n tiene relaci&oacute;n directa con la falla reportada.</p>
<p>Si se instalan en el veh&iacute;culo, repuestos o accesorios no autorizados por Daimler Colombia S.A. y los da&ntilde;os tienen relaci&oacute;n directa o indirecta con los montajes referidos.</p>
<p>Si fuesen constatadas violaciones o alteraciones al veloc&iacute;metro, od&oacute;metro (cuenta kil&oacute;metros) &nbsp;o si se observara que el mismo ha permanecido fuera de funcionamiento con el fin de obtener el beneficio de garant&iacute;a.</p>
<p>Elementos o piezas que hayan sido intervenidas o reparadas por colisi&oacute;n, ya que estas estar&aacute;n cubiertas por la garant&iacute;a de piezas o reparaci&oacute;n. De igual forma no tendr&aacute;n garant&iacute;a los veh&iacute;culos declarados en p&eacute;rdida total o que sean salvamento.</p>
<p>Da&ntilde;os en tapices y revestimientos internos relacionados con desgaste por el uso normal de estas piezas, al igual que deformaciones, decoloraciones y manchas ocasionadas por excesiva exposici&oacute;n al sol despu&eacute;s de 12 meses o 20.000 Km (lo primero que ocurra).</p>
<p>Ajustes al veh&iacute;culo: estos trabajos por no involucrar cambio de piezas (entonaci&oacute;n, ajuste de suspensi&oacute;n, ajuste de frenos, etc) no son cubiertos por la garant&iacute;a, ya que se generan a consecuencia del uso normal del veh&iacute;culo, Los ruidos y rechinidos que no sean consecuencia de un da&ntilde;o o desajuste mec&aacute;nico tendr&aacute;n cobertura hasta los 12 meses o 20.000 km (lo primero que ocurra).</p>
<h3>MANTENIMIENTO</h3>
<p>La presente garant&iacute;a no cubre costos de mantenimiento de acuerdo con lo establecido en las instrucciones de servicio y conservaci&oacute;n del veh&iacute;culo, o&nbsp; fallas debidas al uso de combustible, lubricantes y refrigerantes que no cumplan con las especificaciones recomendadas por Daimler Colombia S.A.</p>
<p>La realizaci&oacute;n de los mantenimientos establecidos en el manual del conductor y de mantenimiento, el uso de combustibles, aceites, lubricantes y refrigerantes apropiados es responsabilidad&nbsp; del propietario.</p>
<h3>PERIODO DE COBERTURA&nbsp;</h3>
<p>Esta garant&iacute;a aplica para el primer propietario y los due&ntilde;os subsiguientes y estar&aacute; vigente durante el periodo o el recorrido mencionados a continuaci&oacute;n, contados desde la fecha de entrega del veh&iacute;culo al primer propietario (estipulada en este certificado), y&nbsp; terminar&aacute; cuando se cumpla el plazo o alcance el kilometraje indicado, lo primero que ocurra y aplica para los veh&iacute;culos nuevos, importados o ensamblados por <strong>Daimler Colombia S.A., </strong>vendidos por la compa&ntilde;&iacute;a o por sus &nbsp;concesionarios autorizados matriculados y usados en Colombia.</p>
<br>
<table class="table tabled tablebold">
<tbody>
<tr style="height: 49px;">
<td style="height: 49px;" colspan="2">VIGENCIA DE GARANT&Iacute;A PARA AUTOMOVILES</td>
</tr>
<tr style="height: 49px;">
<td style="height: 49px;">Todas las clases</td>
<td style="height: 49px;">5 a&ntilde;os o 100.000 Km</td>
</tr>
</tbody>
</table >
<br>
<p>La garant&iacute;a de la bater&iacute;a es de 36 meses o 50.000 Km</p>
<p>Todas las intervenciones realizadas al amparo de la Garant&iacute;a Contractual est&aacute;n garantizadas hasta el vencimiento del per&iacute;odo original de garant&iacute;a del veh&iacute;culo (5 a&ntilde;os). Para las reparaciones realizadas a partir del cuarto a&ntilde;o o repuestos comprados por mostrador aplica la siguiente vigencia.</p>
<br>

<table class="table tabled tablebold" >
<tbody>
<tr>
<td>VIGENCIA GARANT&Iacute;A DE REPUESTOS/REPARACI&Oacute;N</td>
</tr>
<tr>
<td>12 meses sin l&iacute;mite de kilometraje</td>
</tr>
<tr>
<td>El plazo y/o kilometraje se contabilizar&aacute;, tomando como base la fecha y/o kilometraje registrado en la Factura</td>
</tr>
</tbody>
</table>
<br>

<h3>PIEZAS O COMPONENTES NO CUBIERTOS POR LA &nbsp;GARANT&Iacute;A</h3>
<p>La presente garant&iacute;a excluye componentes y piezas que presenten desgaste normal o que sean de mantenimiento como: filtros (combustible, lubricantes, admisi&oacute;n de aire, aire acondicionado y calefacci&oacute;n), l&iacute;quidos de llenado (lubricantes, combustible, refrigerante, l&iacute;quido de frenos, fluidos hidr&aacute;ulicos, fluidos del sistema de aire acondicionado, grasas), buj&iacute;as, correas, embrague (disco y prensa) retenedores, guayas, guardapolvos, rotulas, bujes, amortiguadores, soportes motor, fuelles neum&aacute;ticos, muelles y espirales, pastillas de freno, bandas y discos de freno, campanas, empaquetadura de mordazas, elementos el&eacute;ctricos como bombillos, rel&eacute;s, fusibles, accesorios como plumillas, antenas, vidrios, herramienta,&nbsp; gato. Las piezas o sistemas antes mencionados podr&aacute;n ser cubiertos en los casos que se verifique que un defecto de fabricaci&oacute;n o ensamble de la pieza es la causa del evento reportado por el cliente.</p>
<p>La garant&iacute;a tampoco cubre alineaci&oacute;n y balanceo de ruedas, mano de obra, ni el mantenimiento o reajuste de las partes anteriormente mencionadas y otras que est&eacute;n dentro del mismo concepto de desgaste normal o de mantenimiento.</p>
<p>Los neum&aacute;ticos gozan de la garant&iacute;a que otorgan sus fabricantes acogi&eacute;ndose a las condiciones otorgadas por los mismos. En todo caso, las llantas estar&iacute;an exentas en caso que el da&ntilde;o sea consecuencia de golpes, pellizcamientos causados por golpes, desgastes prematuros por falta o fallas en la alineaci&oacute;n y el uso de una presi&oacute;n de inflado incorrecta.</p>
<p>En caso de cualquier reclamaci&oacute;n por Garant&iacute;a, el concesionario de su preferencia est&aacute; a disposici&oacute;n para encaminar su solicitud al representante local de la marca de los neum&aacute;ticos de su veh&iacute;culo.</p>
<h3>PROCEDIMIENTO PARA HACER EFECTIVA LA &nbsp;GARANTIA</h3>
<p>El Propietario podr&aacute; solicitar que se haga efectiva la garant&iacute;a en cualquiera de los talleres o concesionarios autorizados en el territorio colombiano publicados en nuestra p&aacute;gina web <a href="http://www.daimlerchrysler.com.co">www.daimler.com.co</a> y&nbsp; relacionados a continuaci&oacute;n:</p>
<h3>RESPONSABILIDAD</h3>
<p>Daimler Colombia S.A. y las plantas fabricantes de los veh&iacute;culos vendidos por la &nbsp;Compa&ntilde;&iacute;a o por sus concesionarios autorizados del pa&iacute;s, se reservan el derecho de modificar y/o mejorar en cualquier momento&nbsp; las especificaciones de sus productos y el cubrimiento de la garant&iacute;a, sin que por ello contraigan la obligaci&oacute;n de efectuar tales modificaciones o perfeccionamientos en sus productos vendidos anteriormente.</p>
<p>Daimler Colombia S.A. tiene un mecanismo institucional de recepci&oacute;n y tr&aacute;mite de Peticiones, Quejas y Reclamos (PQR), si usted lo requiere puede acudir al Coordinador de Customer Care, quien le explicara nuestro procedimiento interno de PQR. Los datos de contacto son: Coordinador de Customer Care, tel. 5187474 ext. 1642 y/o a los tel&eacute;fonos (1) 742 6850 y el celular es 3162801318 de Bogot&aacute;. La presentaci&oacute;n de PQR&acute;S No tiene que ser personal, ni requiere de intervenci&oacute;n de abogado.</p>
<p>En caso de persistir la inconformidad, le recordamos que puede acudir ante las autoridades competentes</p>
<p>Declaro que he recibido, le&iacute;do completamente, comprendido y aceptado los&nbsp; t&eacute;rminos, las condiciones de la garant&iacute;a y las dem&aacute;s informaciones contenidas en &eacute;ste documento.</p>
</div>
            <?php 
                include "include/lateral-clientes.php";  
            ?>
        
        
    </div>
</div><!-- .inner clientes-->

<!-- MIGA-->
<div class="miga container">
    <div class="row">
        <a href="index.php" title="Ir al inicio">Home</a><span class="sep">></span>
        <a >Clientes</a> <span class="sep">></span>
        <a href="mercedes-benz.php" title="Ir a Mercedes-Benz">Mercedes-Benz</a> 
    </div>
</div>

<?php 
include "include/destacados.php";
include "include/footer.php";
?>      